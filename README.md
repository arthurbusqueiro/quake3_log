# README #

Esse README serve para configuração e execução da API

* Instale o NodeJS (https://nodejs.org/en/download/)
* Com um terminal, navegue até a pasta onde se encontra o projeto extraído
* Rode o comando 'npm install' para instalar as dependências do projeto
* Rode a aplicação com o comando 'npm start'
* Se tudo ocorrer bem, a aplicação estará rodando na porta 'http://localhost:44666/'
* Para verificar o relatório de um jogo, acesse o navegador na seguinte URL:
* http://localhost:44666/logs/result/1
* O número 1 deve ser substituído pelo número do jogo a ser gerado o relatório

# TESTS #
* A API possui testes unitários utilizando a biblioteca Mocha.
* Verifique se a biblioteca e suas dependências estão corretamente instaladas.
* npm install mocha -g --save-dev
* npm install chai --save-dev
* npm install should --save-dev
* npm install request --save-dev
* Utilize o comando 'mocha' para realizar os testes.

### Who do I talk to? ###

* Arthur Busqueiro
* https://bitbucket.org/arthurbusqueiro/
* arthurbusqueiro@gmail.com