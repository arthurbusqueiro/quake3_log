'use strict';

const express = require('express');

const router = express.Router();
const controller = require('../controllers/log-controller');

router.get('/result/:id', controller.getById);
router.get('/all', controller.getAll);

router.get('/', (req, res) => {
  res.status(200).send({
    title: 'get log works!'
  });
});


module.exports = router;
