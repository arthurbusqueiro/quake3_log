'use strict';

const fs = require('fs');

class Util {
  constructor() {}

  //Lê o arquivo games.log e retorna todos os jogos como um JSON
  parseLog() {
    var rows = [];
    var kills = []; 
    var mygame = {
      game: String,
      kills: []
    };
    var result = [];

    //Le o arquivo na variável file
    var file = fs.readFileSync('src/assets/log/games.log', 'utf8', );

    //Coloca cada linha do arquivo em uma Array filtrando por InitGame ou Kill
    rows = file.split('\n').filter(x => x.includes('InitGame') || x.includes('Kill'));

    //Percorre as linhas do arquivo
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];

      //Se for o início de uma partida
      if (row.includes('InitGame')) {

        //Se já existem kills, encerra a partida
        if (kills.length > 0) {

          var totalkills = 0; //Soma de kills na partida
          var players = [];//Lista com nome dos players
          var ks = {}; //Kills de cada player

          kills.forEach(kill => {
            totalkills += kill.kills;

            //Descarta <world> da lista de players e kills
            if (!kill.player.includes('<world>')) {
              players.push(kill.player);
              ks[kill.player] = kill.kills;
            }

          });

          //Adicionar partida na lista de resultados
          result.push({
            total_kills: totalkills,
            players: players,
            kills: ks
          });
          kills = [];
        }
      } else {
        //Se for uma Kill
        if (row.includes('Kill')) {
          var kill = row.split(':');
          kill = kill[kill.length - 1].split(' killed ');
          var killer = kill[0].replace(' ', ''); //Quem matou
          var killed = kill[1].split(' by ')[0]; //Quem foi morto

          //Procura quem matou no array de kills
          var list = kills.find(k => k.player === killer);

          //Se já está na lista, incrementa quantos ele matou na partida
          if (list) {
            if (killer !== killed) {
              list.kills++;
            }
          } 
          else {
            //Senão Adiciona o player no array de kills
            kills.push({
              player: killer,
              kills: 1,
              deaths: 0
            });
          }
          //Procura quem foi morto no array de kills          
          list = kills.find(k => k.player == killed);

          //Se já está na lista, incrementa as mortes na partida          
          if (list) {
            list.deaths++;
          } else {
            //Senão Adiciona o player no array de kills            
            kills.push({
              player: killed,
              kills: 0,
              deaths: 1
            })
          }

          //Se quem matou foi o <world>
          if (killer === '<world>') {
            //Decrementa o número de quantos matou de quem foi morto pelo <world>
            kills.find(x => x.player === killed).kills--;
          }
        }
      }
    };
    
    return result;
  }

}

module.exports = Util;