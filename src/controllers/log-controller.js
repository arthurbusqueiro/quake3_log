'use strict';

const fs = require('fs');
const Util = require('../utils/util');

exports.getById = async (req, res, next) => {
  try {
    const util = new Util();

    //Lê o arquivo e pega a posição do jogo no array retornado
    var result = util.parseLog()[req.params.id - 1];

    //Retorna o resultado como JSON
    res.status(200).json(result);
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};


exports.getAll = async (req, res, next) => {
  try {
    const util = new Util();

    //Lê o arquivo e pega a posição do jogo no array retornado
    var result = util.parseLog();

    //Retorna o resultado como JSON
    res.status(200).json(result);
  } catch (error) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};