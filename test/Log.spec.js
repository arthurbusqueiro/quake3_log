var should = require("should");
var request = require("request");
var chai = require("chai");
var expect = chai.expect;
var urlBase = "http://localhost:44666/";

describe("Teste API Quake3_Log", function () {
    it("Deve retornar o resultado de todos os jogos", function (done) {
        request.get({
                url: urlBase + "logs/all"
            },
            function (error, response, body) {

                // converter o retorno para um objeto json
                var _body = {};
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {};
                }

                // verifica se o resultado da chamada foi sucesso (200)
                expect(response.statusCode).to.equal(200);

                expect(_body).to.have.lengthOf.at.least(18);
                _body.forEach(element => {
                    expect(element).to.have.property('total_kills');
                    expect(element).to.have.property('players');
                    expect(element).to.have.property('kills');
                    expect(element.players).to.not.have.property('<world>');
                    expect(element.kills).to.not.have.property('<world>');
                });

                done(); // avisamos o test runner que acabamos a validacao e ja pode proseeguir
            }
        );
    });

    it("Deve retornar a partida 3 ", function (done) {
        //faremos a chamada com o nome em ingles mesmo, para verificar se eh a carta correta, vamos ver o artista e o nome da carta novamente
        request.get({
                url: urlBase + "logs/result/3"
            },
            function (error, response, body) {

                // precisamos converter o retorno para um objeto json
                var _body = {};
                try {
                    _body = JSON.parse(body);
                } catch (e) {
                    _body = {};
                }

                // sucesso (200)?
                expect(response.statusCode).to.equal(200);

                expect(_body).to.have.property('total_kills');
                expect(_body).to.have.property('players');
                expect(_body).to.have.property('kills');
                expect(_body.players).to.not.have.property('<world>');
                expect(_body.kills).to.not.have.property('<world>');
                expect(_body.total_kills).to.equal(80);
                expect(_body.kills["Isgalamido"]).to.equal(19);
                expect(_body.kills["Dono da Bola"]).to.equal(9);
                expect(_body.kills["Zeh"]).to.equal(20);
                expect(_body.kills["Assasinu Credi"]).to.equal(12);

                done(); // avisamos o test runner que acabamos a validação e já pode prosseguir
            }
        );
    });
});